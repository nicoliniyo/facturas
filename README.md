# README #

Esta aplicación es una implementación para la emisión de facturas computarizadas para impuestos en la región Boliviana.

### Este repositorio contiene un proyecto Java maven ###

* Por ahora esta aplicación solo cubre la generación del código de control
* Version 0.1
* [Sitio Web Impuestos](http://www.impuestos.gob.bo/images/GACCT/FACTURACION/)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Instalación ###

* Descarga el código fuente

```
#!html

https://nicoliniyo@bitbucket.org/nicoliniyo/facturas.git
```

* Configuración (pendiente)
* Dependencias (pendiente)
* Database configuración (en construcción)
* Ejecutar los tests ()
* Ejecutar la aplicación de escritorio (en construcción)


### Contacto ###

* Repo owner or admin
* Other community or team contact