/*
 * VerhoeffTest.
 */

package org.osbosoftware.facturas.core;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.validator.routines.checkdigit.CheckDigitException;
import org.apache.commons.validator.routines.checkdigit.VerhoeffCheckDigit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nabruzzese
 */
public class VerhoeffTest {
    
    public VerhoeffTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of obtener method, of class Verhoeff.
     * Datos obtenidos del documento del SNI.
     */
    @Test
    public void testObtener() {
        System.out.println("obtener");
        Verhoeff instance = new Verhoeff();
        int obtener1 = instance.obtener("12083");
        int obtener2 = instance.obtener("0");
        int obtener3 = instance.obtener("1810");
        int obtener4 = instance.obtener("04");
        
        System.out.println(obtener1);
        System.out.println(obtener2);
        System.out.println(obtener3);
        System.out.println(obtener4);
        assertEquals(7, obtener1);
        assertEquals(4, obtener2);
        assertEquals(8, obtener3);
        assertEquals(7, obtener4);
        
    }
    
    @Test
    public void testCommonsVerhoeff() {
        try {
            System.out.println("ApacheVerhoeef");
            org.apache.commons.validator.routines.checkdigit.VerhoeffCheckDigit v = new VerhoeffCheckDigit();
            String calculate1 = v.calculate("12083");
            String calculate2 = v.calculate("0");
            String calculate3 = v.calculate("1810");
            String calculate4 = v.calculate("04");
            
            
            System.out.println(calculate1);
            System.out.println(calculate2);
            System.out.println(calculate3);
            System.out.println(calculate4);
            assertEquals("7", calculate1);
            assertEquals("4", calculate2);
            assertEquals("8", calculate3);
            assertEquals("7", calculate4);
            
        } catch (CheckDigitException ex) {
            Logger.getLogger(VerhoeffTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
