/*
 * AllegedRC4Test.
 */

package org.osbosoftware.facturas.core;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nabruzzese
 */
public class AllegedRC4Test {
    
    public AllegedRC4Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of encriptaSinguion method, of class AllegedRC4.
     */
    @Test
    public void testEncriptaSinguion() {
        System.out.println("encriptaSinguion");
        String mensaje = "";
        String llave = "";
        AllegedRC4 instance = new AllegedRC4();
        String expResult = "";
//        String result = instance.encriptaSinguion(mensaje, llave);
    }

    /**
     * Test of encripta method, of class AllegedRC4.
     * 
     *  CifrarMensajeRC4 (“d3Ir6”, “sesamo”) Resultado: CadenaCifrada = EB-06-AE-F8-92
     *  CifrarMensajeRC4 (“piWCp”, “Aa1-bb2-Cc3-Dd4”) Resultado: CadenaCifrada = 37-71-2E-14-A0
     *  CifrarMensajeRC4 (“IUKYo”, “XBCPY-GKGX4-PGK44-8B632-X9P33”) Resultado: CadenaCifrada = 83-62-FC-B0-F0
     */
    @Test
    public void testEncripta() {
        System.out.println("encripta");
        String mensaje = "d3Ir6";
        String llave = "sesamo";
        String mensaje1 = "piWCp";
        String llave1 = "Aa1-bb2-Cc3-Dd4";
        String mensaje2 = "IUKYo";
        String llave2 = "XBCPY-GKGX4-PGK44-8B632-X9P33";
        AllegedRC4 instance = new AllegedRC4();
        String expResult = "EB-06-AE-F8-92";
        String expResult1 = "37-71-2E-14-A0";
        String expResult2 = "83-62-FC-B0-F0";
        instance.setMensajeYLllave(mensaje, llave);
        String result = instance.cifrar();
        instance.setMensajeYLllave(mensaje1, llave1);
        String result1 = instance.cifrar();
        instance.setMensajeYLllave(mensaje2, llave2);
        String result2 = instance.cifrar();
        System.out.println("Prueba 1: " + result); System.out.println("Obtenido: EB-06-AE-F8-92 ");
        System.out.println("Prueba 2: " + result1); System.out.println("Obtenido: 37-71-2E-14-A0 ");
        System.out.println("Prueba 4: " + result2); System.out.println("Obtenido: 83-62-FC-B0-F0 ");
        assertEquals(expResult, result);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        
        
    }
    
    @Test
    public void testEjm1(){
        String mensaje = "290400110079rCB7Sv4150312X24189179011589d)5k7N2007070201%3a250031b8";
        String llave = "9rCB7Sv4X29d)5k7N%3ab89p-3(5[A71621";
        AllegedRC4 rc4 = new AllegedRC4();
        rc4.setMensajeYLllave(mensaje, llave);
        String cifrar = rc4.cifrar();
        System.out.println(cifrar);
        String expRes = "69DD0A42536C9900C4AE6484726C122ABDBF95D80A4BA403FB7834B3EC2A88595E2149A3D965923BA4547B42B9528AAE7B8CFB9996BA2B58516913057C9D791B6B748A";
        expRes = guionizar(expRes);
        System.out.println((expRes));
        assertEquals(expRes, cifrar);
    }
    
    @Test
    public void testEjm2(){
        String mensaje = "79040011859A3Fs415272s$)1026469026922cvD(eY6200707286867A5C4A135412rsdf5";
        String llave = "A3Fs4s$)2cvD(eY667A5C4A2rsdf53kw9654E2B23s24df35F542765";
        AllegedRC4 rc4 = new AllegedRC4();
        rc4.setMensajeYLllave(mensaje, llave);
        String cifrar = rc4.cifrar();
        System.out.println(cifrar);
        String expRes = "BEE6B80BB6F414D9AE3031EFA37C272B9B6CB87EFE32C4407296FA4CA7825E85ADDA18CA746CA83A6ABC6E8527B6C487CB9BBCF9A6AF59F22FB6A0934C5ED94301A02C5484E48BBD";
        expRes = guionizar(expRes);
        System.out.println((expRes));
        assertEquals(expRes, cifrar);
    }
    
    @Test
    public void testEjm3Valido(){
        String mensaje = "79040011859A3Fs415272s$)1026469026922cvD(eY6200707286867A5C4A135412rsdf5";
        String llave = "A3Fs4s$)2cvD(eY667A5C4A2rsdf53kw9654E2B23s24df35F542765";
        AllegedRC4 rc4 = new AllegedRC4();
        rc4.setMensajeYLllave(mensaje, llave);
        String cifrar = rc4.cifrar();
        System.out.println(cifrar);
        String expRes = "BEE6B80BB6F414D9AE3031EFA37C272B9B6CB87EFE32C4407296FA4CA7825E85ADDA18CA746CA83A6ABC6E8527B6C487CB9BBCF9A6AF59F22FB6A0934C5ED94301A02C5484E48BBD";
        expRes = guionizar(expRes);
        System.out.println((expRes));
        assertEquals(expRes, cifrar);
    }
    
    /**
     * Convertir las cadenas obtenidas a cadenas spearadas x guiones segun especs.
     * @param linea
     * @return 
     */
    private String guionizar(String linea){
        int largo = linea.length();
        StringBuilder cadena = new StringBuilder();
        for(int i = 0; i <= largo; i+=2){
            if(i <= largo-3){
                cadena.append(linea.substring(i, i+2)).append("-");
            }
            if(i+2 == largo){
                cadena.append(linea.substring(i, i+2));
            }
        }
        
        return cadena.toString();
    }
    
    @Test
    public void testGuionizar(){
        String txt = "AABBCCDD";
        String exp = "AA-BB-CC-DD";
        String guionizado = guionizar(txt);
        System.out.println(guionizado);
        assertEquals(exp.trim(), guionizado.trim());
    }
    
        
    }
