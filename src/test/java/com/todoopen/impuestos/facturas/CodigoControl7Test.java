/*
 * CodigoControl7Test.
 */

package com.todoopen.impuestos.facturas;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;
import org.apache.commons.validator.routines.checkdigit.CheckDigitException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author nabruzzese
 */
public class CodigoControl7Test extends TestCase {
    
    public CodigoControl7Test(String testName) {
        super(testName);
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    

    /**
     * Test of getNumeroFactura method, of class CodigoControl7.
     */
    public void testGetNumeroFactura() {
        System.out.println("getNumeroFactura");
        CodigoControl7 instance = new CodigoControl7();
        long expResult = 0L;
//        long result = instance.getNumeroFactura();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of setNumeroFactura method, of class CodigoControl7.
     */
    public void testSetNumeroFactura() {
        System.out.println("setNumeroFactura");
        long numeroFactura = 0L;
//        CodigoControl7 instance = new CodigoControl7();
//        instance.setNumeroFactura(numeroFactura);
        
        
    }

    /**
     * Test of getNitci method, of class CodigoControl7.
     */
    public void testGetNitci() {
        System.out.println("getNitci");
        CodigoControl7 instance = new CodigoControl7();
//        String expResult = "";
//        String result = instance.getNitci();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of setNitci method, of class CodigoControl7.
     */
    public void testSetNitci() {
        System.out.println("setNitci");
        String nitci = "";
//        CodigoControl7 instance = new CodigoControl7();
//        instance.setNitci(nitci);
        
        
    }

    /**
     * Test of getFechaTransaccion method, of class CodigoControl7.
     */
    public void testGetFechaTransaccion() {
        System.out.println("getFechaTransaccion");
        CodigoControl7 instance = new CodigoControl7();
//        Date expResult = null;
//        Date result = instance.getFechaTransaccion();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of setFechaTransaccion method, of class CodigoControl7.
     */
    public void testSetFechaTransaccion() {
        System.out.println("setFechaTransaccion");
        Date fechaTransaccion = null;
//        CodigoControl7 instance = new CodigoControl7();
//        instance.setFechaTransaccion(fechaTransaccion);
        
        
    }

    /**
     * Test of getFechaTransaccionStr method, of class CodigoControl7.
     */
    public void testGetFechaTransaccionStr() {
        System.out.println("getFechaTransaccionStr");
        CodigoControl7 instance = new CodigoControl7();
        String expResult = "";
//        String result = instance.getFechaTransaccionStr();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of getMonto method, of class CodigoControl7.
     */
    public void testGetMonto() {
        System.out.println("getMonto");
        CodigoControl7 instance = new CodigoControl7();
        BigDecimal expResult = null;
//        BigDecimal result = instance.getMonto();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of setMonto method, of class CodigoControl7.
     */
    public void testSetMonto() {
        System.out.println("setMonto");
        BigDecimal monto = null;
        CodigoControl7 instance = new CodigoControl7();
//        instance.setMonto(monto);
        
        
    }

    /**
     * Test of getLlaveDosificacion method, of class CodigoControl7.
     */
    public void testGetLlaveDosificacion() {
        System.out.println("getLlaveDosificacion");
        CodigoControl7 instance = new CodigoControl7();
        String expResult = "";
//        String result = instance.getLlaveDosificacion();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of setLlaveDosificacion method, of class CodigoControl7.
     */
    public void testSetLlaveDosificacion() {
        System.out.println("setLlaveDosificacion");
        String llaveDosificacion = "";
//        CodigoControl7 instance = new CodigoControl7();
//        instance.setLlaveDosificacion(llaveDosificacion);
        
        
    }

    /**
     * Test of obtener method, of class CodigoControl7.
     */
    public void testObtener() throws Exception {
        System.out.println("obtener");
        CodigoControl7 instance = new CodigoControl7();
        String expResult = "";
//        String result = instance.obtener();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of getNumeroAutorizacion method, of class CodigoControl7.
     */
    public void testGetNumeroAutorizacion() {
        System.out.println("getNumeroAutorizacion");
        CodigoControl7 instance = new CodigoControl7();
        String expResult = "";
//        String result = instance.getNumeroAutorizacion();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of setNumeroAutorizacion method, of class CodigoControl7.
     */
    public void testSetNumeroAutorizacion() {
        System.out.println("setNumeroAutorizacion");
        String numeroAutorizacion = "";
        CodigoControl7 instance = new CodigoControl7();
//        instance.setNumeroAutorizacion(numeroAutorizacion);
        
        
    }

    /**
     * Test of obtieneAscii method, of class CodigoControl7.
     */
    public void testObtieneAscii() {
        System.out.println("obtieneAscii");
        char valor = ' ';
        CodigoControl7 instance = new CodigoControl7();
        int expResult = 0;
//        int result = instance.obtieneAscii(valor);
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of getDebug method, of class CodigoControl7.
     */
    public void testGetDebug() {
        System.out.println("getDebug");
        CodigoControl7 instance = new CodigoControl7();
        Level expResult = null;
//        Level result = instance.getDebug();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of setDebug method, of class CodigoControl7.
     */
    public void testSetDebug() {
        System.out.println("setDebug");
        Level debug = null;
        CodigoControl7 instance = new CodigoControl7();
        instance.setDebug(debug);
        
        
    }

    /**
     * Test of getModoRedondeo method, of class CodigoControl7.
     */
    public void testGetModoRedondeo() {
        System.out.println("getModoRedondeo");
        CodigoControl7 instance = new CodigoControl7();
//        RoundingMode expResult = null;
//        RoundingMode result = instance.getModoRedondeo();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of setModoRedondeo method, of class CodigoControl7.
     */
    public void testSetModoRedondeo() {
        System.out.println("setModoRedondeo");
        RoundingMode modoRedondeo = null;
        CodigoControl7 instance = new CodigoControl7();
        instance.setModoRedondeo(modoRedondeo);
        
        
    }

    /**
     * Test of getDecRedondeo method, of class CodigoControl7.
     */
    public void testGetDecRedondeo() {
        System.out.println("getDecRedondeo");
        CodigoControl7 instance = new CodigoControl7();
//        int expResult = 0;
//        int result = instance.getDecRedondeo();
//        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of setDecRedondeo method, of class CodigoControl7.
     */
    public void testSetDecRedondeo() {
        System.out.println("setDecRedondeo");
        int decRedondeo = 0;
        CodigoControl7 instance = new CodigoControl7();
        instance.setDecRedondeo(decRedondeo);
        
        
    }
    
    /**
     * Test of obtieneAscii method, of class CodigoControl7.
     */
    @Test
    public void testCC7() {
        try {
            
            CodigoControl7 cc7 = new CodigoControl7();
            cc7.setNumeroAutorizacion("29040011007");
            cc7.setNumeroFactura(1503);
            cc7.setNitci("4189179011");
            Date fecha = new Date();
            String formato = "yyyyMMdd";
            SimpleDateFormat sdf = new SimpleDateFormat(formato);
            fecha = sdf.parse("20070702");
            cc7.setFechaTransaccion(fecha);
            cc7.setMonto(new BigDecimal("2500"));
            cc7.setLlaveDosificacion("9rCB7Sv4X29d)5k7N%3ab89p-3(5[A");
            String cc = cc7.obtener();
            System.out.println("resultado: " + cc);
            assertEquals("6A-DC-53-05-14", cc);
            System.out.println("--------------------------");
           
        } catch (ParseException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CheckDigitException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    /**
     * Test CodigoControl7 Ejm1.
     */
    @Test
    public void testCC7Ejm1() {
        try {
            
            
            CodigoControl7 cc7 = new CodigoControl7();
            cc7.setNumeroAutorizacion("79040011859");
            cc7.setNumeroFactura(152);
            cc7.setNitci("1026469026");
            Date fecha = new Date();
            String formato = "yyyyMMdd";
            SimpleDateFormat sdf = new SimpleDateFormat(formato);
            fecha = sdf.parse("20070728");
            cc7.setFechaTransaccion(fecha);
            cc7.setMonto(new BigDecimal("135"));
            cc7.setLlaveDosificacion("A3Fs4s$)2cvD(eY667A5C4A2rsdf53kw9654E2B23s24df35F5");
            String cc = cc7.obtener();
            System.out.println("resultado: " + cc);
            assertEquals("FB-A6-E4-78", cc);
            System.out.println("--------------------------");
                    
        } catch (ParseException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CheckDigitException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    /**
     * Test CodigoControl7 Ejm2.
     */
    @Test
    public void testCC7Ejm2() {
        try {
            CodigoControl7 cc7 = new CodigoControl7();
            cc7.setNumeroAutorizacion("20040010113");
            cc7.setNumeroFactura(665);
            cc7.setNitci("1004141023");
            Date fecha = new Date();
            String formato = "yyyyMMdd";
            SimpleDateFormat sdf = new SimpleDateFormat(formato);
            fecha = sdf.parse("20070108");
            cc7.setFechaTransaccion(fecha);
            cc7.setMonto(new BigDecimal("905.23"));
            cc7.setLlaveDosificacion("442F3w5AggG7644D737asd4BH5677sasdL4%44643(3C3674F4");
            String cc = cc7.obtener();
            System.out.println("resultado: " + cc);
            assertEquals("71-D5-61-C8", cc);
            System.out.println("--------------------------");
                    
        } catch (ParseException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CheckDigitException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Test CodigoControl7 Ejm3.
     */
    @Test
    public void testCC7Ejm3() {
        try {
            CodigoControl7 cc7 = new CodigoControl7();
            cc7.setNumeroAutorizacion("1904008691195");
            cc7.setNumeroFactura(978256);
            cc7.setNitci("0");
            Date fecha = new Date();
            String formato = "yyyyMMdd";
            SimpleDateFormat sdf = new SimpleDateFormat(formato);
            fecha = sdf.parse("20080201");
            cc7.setFechaTransaccion(fecha);
            cc7.setMonto(new BigDecimal("26006"));
            cc7.setLlaveDosificacion("pPgiFS%)v}@N4W3aQqqXCEHVS2[aDw_n%3)pFyU%bEB9)YXt%xNBub4@PZ4S9)ct");
            String cc = cc7.obtener();
            System.out.println("resultado: " + cc);
            assertEquals("62-12-AF-1B", cc);
            System.out.println("--------------------------");
                    
        } catch (ParseException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CheckDigitException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Test CodigoControl7 Ejm4.
     */
    @Test
    public void testCC7Ejm4() {
        try {
            CodigoControl7 cc7 = new CodigoControl7();
            cc7.setNumeroAutorizacion("10040010640");
            cc7.setNumeroFactura(9901);
            cc7.setNitci("1035012010");
            Date fecha = new Date();
            String formato = "yyyyMMdd";
            SimpleDateFormat sdf = new SimpleDateFormat(formato);
            fecha = sdf.parse("20070813");
            cc7.setFechaTransaccion(fecha);
            cc7.setMonto(new BigDecimal("451.49"));
            cc7.setLlaveDosificacion("DSrCB7Ssdfv4X29d)5k7N%3ab8p3S(asFG5YU8477SWW)FDAQA");
            String cc = cc7.obtener();
            System.out.println("resultado: " + cc);
            assertEquals("6A-50-31-01-32", cc);
            System.out.println("--------------------------");
                    
        } catch (ParseException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CheckDigitException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Test CodigoControl7 Ejm5.
     * Este ejemplo tiene como objetivo demostrar el redondeo, 5725.90 a 5726.
     */
    @Test
    public void testCC7Ejm5() {
        try {
            CodigoControl7 cc7 = new CodigoControl7();
            cc7.setNumeroAutorizacion("30040010595");
            cc7.setNumeroFactura(10015);
            cc7.setNitci("953387014");
            Date fecha = new Date();
            String formato = "yyyyMMdd";
            SimpleDateFormat sdf = new SimpleDateFormat(formato);
            fecha = sdf.parse("20070825");
            cc7.setFechaTransaccion(fecha);
            cc7.setMonto(new BigDecimal("5725.90"));
            cc7.setLlaveDosificacion("33E265B43C4435sdTuyBVssD355FC4A6F46sdQWasdA)d56666fDsmp9846636B3");
            String cc = cc7.obtener();
            System.out.println("resultado: " + cc);
            assertEquals("A8-6B-FD-82-16", cc);
            System.out.println("--------------------------");
                    
        } catch (ParseException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CheckDigitException ex) {
            Logger.getLogger(CodigoControl7Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
