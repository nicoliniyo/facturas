/*
 *  clase de Alleged RC4 encriptacion
 */
package org.osbosoftware.facturas.core;

import java.util.logging.Logger;

/**
 *
 * @author Creador Osbosoftware
 */
public class AllegedRC4 {
    
    private String mensaje;
    
    private String llave;

    /**
     * Constructor generico.
     */
    public AllegedRC4() {
    }
    
    /**
     * Cifrado AlledRC4.
     * @param mensaje
     * @param llave 
     */
    public AllegedRC4(String mensaje, String llave) {
        this.mensaje = mensaje;
        this.llave = llave;
    }

    /**
     * Cifra utilizando los prametros mensaje y llave.
     * @return 
     */
    public String cifrar() {
        return encriptaSinguion(mensaje, llave);
    }
    
    public void setMensajeYLllave(String mensaje, String llave){
        this.mensaje = mensaje;
        this.llave = llave;
    }
    
    /**
     * Remueve el caracter "-" si al inicio de la cadena existe.
     * @param mensaje
     * @param llave
     * @return 
     */
    private String encriptaSinguion(String mensaje,String llave){
        String resul="";
        resul=this.encripta(mensaje, llave);
        if( resul.toCharArray()[0] == '-'){
            System.out.println("Guion al inicio, remplazando!");
            resul=resul.replace("-","");
        }
        
        return resul;
    }
    
    /**
     * AllegedRC4.
     * @param mensaje
     * @param llave
     * @return 
     */
    private String encripta(String mensaje, String llave){
        char[] key = llave.toCharArray();
        int state[]=new int[256];
        int index1=0;
        int index2=0;
        int x=0;
        int y=0;
        int nmen=0;
        String cifrado="";
        System.out.println("State length: " + state.length);
        for (int i=0;i<256;i++){
            state[i]=i;
          //  System.out.print(" " + i + " ");
        }
        System.out.println("");
        //System.out.println(("Llave indx size: " + llave.toCharArray().length));
        for (int o=0;o<256;o++){
            try{
            //System.out.print(("Llave-Char: " + key[index1]));
            } catch (ArrayIndexOutOfBoundsException e) {
                //System.out.println("----------------------AIOB: Index nro: " + index1);
                
            }
            //System.out.print((" Index1: " + String.valueOf(index1) ));
            //System.out.println(" State: " + state[o]);
            index2=(obtieneAscii(key[index1])+state[o]+index2) % 256;
            int aux;
            //intercambiando valores
            aux=state[o];
            state[o]=state[index2];
            state[index2]=aux;
            index1=(index1+1) % llave.length();
        }
        int uno=0;
        int dos=0;
        for(int u=0;u<mensaje.length();u++){
            x=(x+1) % 256;
            y=(state[x]+y) % 256;
            //intercambiando valor
            int aux2;
            aux2=state[x];
            state[x]=state[y];
            state[y]=aux2;
            uno=obtieneAscii(mensaje.toCharArray()[u]);
            dos=state[(state[x]+state[y]) % 256];
            nmen=uno^dos;
            cifrado=cifrado+"-"+rellenaCero(decimalaHexadecimal(nmen));

        }
        String Resultado;
        Resultado=cifrado.substring(1,cifrado.length());
        return Resultado;
    }
    
    /**
     * Devuelve el valor ASCII del caracter.
     * @param valor
     * @return 
     */
    private int obtieneAscii(char valor){
        return (int) valor;
    }
    
    /**
     * En caso de obtener resultados de 1 solo digito, agrega un 0 al inicio.
     * Ejm: 1 -> 01, F -> 0F
     * @param valor
     * @return 
     */
    private String rellenaCero(String valor){
        if (valor.length()==1){
            valor="0"+valor;
        }
        return valor;
    }
    
    /**
     * Convierte un valor a Hex.
     * @param valor
     * @return 
     */
    private String decimalaHexadecimal(int valor){
        return Integer.toHexString(valor).toUpperCase();
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }
    
}
