/*
 * CodigoControl7
 */
package com.todoopen.impuestos.facturas;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.validator.routines.checkdigit.CheckDigitException;
import org.apache.commons.validator.routines.checkdigit.VerhoeffCheckDigit;
import org.osbosoftware.facturas.core.AllegedRC4;
import org.osbosoftware.facturas.core.Base64;

/**
 *
 * @author Creador Osbosoftware
 */
public class CodigoControl7 {

    private String numeroAutorizacion;
    private long numeroFactura;
    private String nitci;
    private Date fechaTransaccion;
    private String fechaTransaccionStr;
    private BigDecimal monto;
    private String llaveDosificacion;
    
    /**
     * Tipo de redondeo utilizado para el monto.
     */
    private RoundingMode modoRedondeo = RoundingMode.HALF_EVEN;
    
    /**
     * Numero de decimales para el redodneo, 0 = se redondeara a enteros.
     */
    private int decRedondeo = 0;
    
    /**
     * Habilitados INFO y FINE.
     */
    private Level debug = Level.INFO;
    
    private static final Logger log = Logger.getLogger(CodigoControl7.class.getName());
    
    public CodigoControl7() {
        log.setLevel(debug);
    }

    public CodigoControl7(String numeroAutorizacion, long numeroFactura, String nitci, String fechaTransaccionStr, 
            BigDecimal monto, String llaveDosificacion) {
        
        log.setLevel(debug);
        
        this.numeroAutorizacion = numeroAutorizacion;
        this.numeroFactura = numeroFactura;
        this.nitci = nitci;
        this.fechaTransaccionStr = fechaTransaccionStr;
        this.monto = monto;
        this.llaveDosificacion = llaveDosificacion;
    }
    
    /**
     * @return the numeroFactura
     */
    public long getNumeroFactura() {
        return numeroFactura;
    }

    /**
     * @param numeroFactura the numeroFactura to set
     */
    public void setNumeroFactura(long numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    /**
     * @return the nitci
     */
    public String getNitci() {
        return nitci;
    }

    /**
     * @param nitci the nitci to set
     */
    public void setNitci(String nitci) {
        this.nitci = nitci;
    }

    /**
     * @return the fechaTransaccion
     */
    public Date getFechaTransaccion() {
        return fechaTransaccion;
    }

    /**
     * @param fechaTransaccion the fechaTransaccion to set
     */
    public void setFechaTransaccion(Date fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    /**
     * @return the fechaTransaccionStr
     */
    public String getFechaTransaccionStr() {
        Date fecha = this.getFechaTransaccion();
        String formato = "yyyyMMdd";
        SimpleDateFormat sdf =
                new SimpleDateFormat(formato);
        String resul = sdf.format(fecha);
        return resul;
    }

    /**
     * @return the monto
     */
    public BigDecimal getMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(BigDecimal monto) {
        BigDecimal mntRedondeado = monto.setScale(decRedondeo, modoRedondeo);
        System.out.println("Redoneado a: " + mntRedondeado);
        this.monto = mntRedondeado;
    }

    /**
     * @return the llaveDosificacion
     */
    public String getLlaveDosificacion() {
        return llaveDosificacion;
    }

    /**
     * @param llaveDosificacion the llaveDosificacion to set
     */
    public void setLlaveDosificacion(String llaveDosificacion) {
        this.llaveDosificacion = llaveDosificacion;
    }

    /**
     * @return retorna el codigo de control generado para la factura
     */
    public String obtener() throws CheckDigitException, NumberFormatException {
        // primer paso
        String numAutorizacion = this.getNumeroAutorizacion();
        String numFactura = String.valueOf(this.getNumeroFactura());
        String numNitci = this.getNitci();
        String numFechaTransaccion = this.getFechaTransaccionStr();
        String numMonto = String.valueOf(this.getMonto());
        String numLlaveDosificacion = this.getLlaveDosificacion();

        VerhoeffCheckDigit ver = new VerhoeffCheckDigit();

        // primero numero

        //System.out.println("NumFactura: " + numFactura);
        numFactura = numFactura + ver.calculate(numFactura);
        //System.out.println("NumFactura + Verhoeff: " + numFactura);
        
        //System.out.println("NumNITCI: " + numNitci);
        numNitci = numNitci + ver.calculate(numNitci);
        //System.out.println("NumNITCI + Verhoeff: " + numNitci);
        
        numFechaTransaccion = numFechaTransaccion + ver.calculate(numFechaTransaccion);
        //System.out.println("NumFecha + Verhoeff: " + numFechaTransaccion);
        numMonto = numMonto + ver.calculate(numMonto);
        //numLlaveDosificacion = numLlaveDosificacion + ver.calculate(numLlaveDosificacion);
        System.out.println("[1]NumMonto + Verhoeff: " + numMonto);

        /*
         * Obteniendio el segundo numero de Verhoeff
        */
        //System.out.println("NumFactura: " + numFactura);
        numFactura = numFactura + ver.calculate(numFactura);
        System.out.println("NumFactura + Verhoeff: " + numFactura);
        
        numNitci = numNitci + ver.calculate(numNitci);
        System.out.println("NumNITCI + Verhoeff: " + numNitci);
        numFechaTransaccion = numFechaTransaccion + ver.calculate(numFechaTransaccion);
        System.out.println("NumFecha + Verhoeff: " + numFechaTransaccion);
        
        numMonto = numMonto + ver.calculate(numMonto);
        //numLlaveDosificacion = numLlaveDosificacion + ver.calculate(numLlaveDosificacion);
        System.out.println("[2]NumMonto + Verhoeff: " + numMonto);

        long sumatoria = Long.parseLong(numFactura) + Long.parseLong(numNitci) + Long.parseLong(numFechaTransaccion) + Long.parseLong(numMonto);
        String sumaStr = String.valueOf(sumatoria);
        System.out.println("Sumatoria: " + sumatoria);
        // hallamos 5 numero verhoff
        sumaStr = sumaStr + ver.calculate(sumaStr);
        sumaStr = sumaStr + ver.calculate(sumaStr);
        sumaStr = sumaStr + ver.calculate(sumaStr);
        sumaStr = sumaStr + ver.calculate(sumaStr);
        sumaStr = sumaStr + ver.calculate(sumaStr);
        
        // 5 digitos
        String digitos = sumaStr.substring(sumaStr.length() - 5, sumaStr.length());
        System.out.println("5 de Verhoof: " + digitos);
        //System.out.println("5 de Verhoof: " + sumaStr.substring(sumaStr.length() - 5, sumaStr.length()));
        //paso 2
        int uno = Integer.parseInt(digitos.substring(0, 1)) + 1;
        int dos = Integer.parseInt(digitos.substring(1, 2)) + 1;
        int tres = Integer.parseInt(digitos.substring(2, 3)) + 1;
        int cuatro = Integer.parseInt(digitos.substring(3, 4)) + 1;
        int cinco = Integer.parseInt(digitos.substring(4, 5)) + 1;
        System.out.println("Suma 1 a c/digito de verhoeff:");
        System.out.println(uno + "-" + dos + "-" + tres + "-" + cuatro + "-" + cinco);

        System.out.println("Llave dosificacion:");
        System.out.println(this.getLlaveDosificacion());
        String cuno = this.getLlaveDosificacion().substring(0, uno);
        System.out.println(cuno);
        
        String cdos = this.getLlaveDosificacion().substring(uno, uno + dos);
        System.out.println(vacios(uno) + cdos);
        
        String ctres = this.getLlaveDosificacion().substring(uno + dos, uno + dos + tres);
        System.out.println(vacios(uno) + vacios(dos) + ctres);
        
        String ccuatro = this.getLlaveDosificacion().substring(uno + dos + tres, uno + dos + tres + cuatro);
        System.out.println(vacios(uno) + vacios(dos) + vacios(tres) + ccuatro);
        
        String ccinco = this.getLlaveDosificacion().substring(uno + dos + tres + cuatro, uno + dos + tres + cuatro + cinco);
        System.out.println(vacios(uno) + vacios(dos) + vacios(tres) + vacios(cuatro) + ccinco);

        
        cuno = this.getNumeroAutorizacion() + cuno;
        cdos = numFactura + cdos;
        ctres = numNitci + ctres;
        ccuatro = numFechaTransaccion + ccuatro;
        ccinco = numMonto + ccinco;
        System.out.println(cuno);
        System.out.println(cdos);
        System.out.println(ctres);
        System.out.println(ccuatro);
        System.out.println(ccinco);
        // paso 3
        String mensaje = cuno + cdos + ctres + ccuatro + ccinco;
        System.out.println("Mensaje:");
        System.out.println(mensaje);
        
        String llave = this.getLlaveDosificacion() + digitos;
        System.out.println("Llave mas digitos");
        System.out.println(llave);
        
        AllegedRC4 ged = new AllegedRC4(mensaje, llave);
        String cifrado = ged.cifrar();
        System.out.println("Cifrado:");
        System.out.println(cifrado);
        cifrado = cifrado.replace("-", "");
        System.out.println("Cifrado limpio: " + cifrado);
        // paso 4 y 5
        int salto = 5;
        int numero0 = 0;
        int numero1 = 0;
        int numero2 = 0;
        int numero3 = 0;
        int numero4 = 0;
        int num = 0;
        for (int u = 0; u < cifrado.length(); u++) {
            if (num == 0) {
                numero0 = this.obtieneAscii(cifrado.toCharArray()[u]) + numero0;
            }
            if (num == 1) {
                numero1 = this.obtieneAscii(cifrado.toCharArray()[u]) + numero1;
            }
            if (num == 2) {
                numero2 = this.obtieneAscii(cifrado.toCharArray()[u]) + numero2;
            }
            if (num == 3) {
                numero3 = this.obtieneAscii(cifrado.toCharArray()[u]) + numero3;
            }
            if (num == 4) {
                numero4 = this.obtieneAscii(cifrado.toCharArray()[u]) + numero4;
            }
            num++;
            if (num == 5) {
                num = 0;
            }
        }
        int total = numero0 + numero1 + numero2 + numero3 + numero4;
        System.out.println("Paso4 Total: " + total);
        System.out.println("Paso4 1: " + numero0);
        System.out.println("Paso4 2: " + numero1);
        System.out.println("Paso4 3: " + numero2);
        System.out.println("Paso4 4: " + numero3);
        System.out.println("Paso4 4: " + numero4);
        //paso 5

        numero0 = (total * numero0) / uno;
        numero1 = (total * numero1) / dos;
        numero2 = (total * numero2) / tres;
        numero3 = (total * numero3) / cuatro;
        numero4 = (total * numero4) / cinco;
        
        
        System.out.println("Paso5 1: " + numero0);
        System.out.println("Paso5 2: " + numero1);
        System.out.println("Paso5 3: " + numero2);
        System.out.println("Paso5 4: " + numero3);
        System.out.println("Paso5 4: " + numero4);
        

        int nuevoTotal = numero0 + numero1 + numero2 + numero3 + numero4;
        System.out.println("Paso5 NuevoTotal: " + nuevoTotal);
        Base64 b = new Base64(nuevoTotal);
        String enbase = b.getResultado();
        System.out.println("B64: " + enbase);
        //sexto paso
        String nuevaLlave= this.getLlaveDosificacion() + digitos;
        String nuevoMensaje  = enbase;
        System.out.println("5 Digitos de Verhoeef: " + digitos);
        System.out.println("Nueva llave: " + nuevaLlave);
        System.out.println("Mensaje Utilizado: " + nuevoMensaje);
        System.out.println("Lave Utilizada: " + nuevaLlave);
        
        
        AllegedRC4 ge = new AllegedRC4(nuevoMensaje,nuevaLlave);
        String codigoControl = ge.cifrar();
        return codigoControl;
    }
    
    /**
     * Genera espacios vacios solo para seguimeinto del proceos.
     * @param nro
     * @return 
     */
    private String vacios(int nro){
        StringBuilder espacios = new StringBuilder();
        for(int i = 0; i < nro ; i++){
            espacios.append(" ");
        }
        return espacios.toString();
    }

    /**
     * @return the numeroAutorizacion
     */
    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    /**
     * @param numeroAutorizacion the numeroAutorizacion to set
     */
    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    /**
     * Obtiene valor int del carater ASCII.
     * @param valor
     * @return 
     */
    public int obtieneAscii(char valor) {
        return (int) valor;
    }

    /**
     * 
     * @return muestra mas detalle en el log.
     */
    public Level getDebug() {
        return debug;
    }

    /**
     * 
     * @param debug asigna INFO o FINE.
     */
    public void setDebug(Level debug) {
        this.debug = debug;
    }

    /**
     * 
     * @return el modo en que se redondea el monto.
     */
    public RoundingMode getModoRedondeo() {
        return modoRedondeo;
    }

    /**
     * 
     * @param modoRedondeo por defecto utiliza RoundingMode.HALF_EVEN.
     */
    public void setModoRedondeo(RoundingMode modoRedondeo) {
        this.modoRedondeo = modoRedondeo;
    }

    /**
     * 
     * @return numero de decimales a ser redondeados, si 0 entonces redondea a unidades.
     */
    public int getDecRedondeo() {
        return decRedondeo;
    }

    /**
     * 
     * @param decRedondeo asigna el numero de decimales a ser redondeados, si 0 entonces redonea a unidades.
     */
    public void setDecRedondeo(int decRedondeo) {
        this.decRedondeo = decRedondeo;
    }
    
}
